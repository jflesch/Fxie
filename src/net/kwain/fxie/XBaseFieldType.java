package net.kwain.fxie;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Vector;

public interface XBaseFieldType {
    public byte getByteValue();
    public String toString();
    public int getMaxInternalLength();
    public int getMaxUserLength();
    public XBaseValue read(XBaseHeader header, XBaseHeader.XBaseField field, DataInput fd) throws XBaseException;
    /**
     * User must call XBaseField.write(), not directly this one. This one is
     * called by XBaseField internally.
     */
    public void write(XBaseHeader header, XBaseHeader.XBaseField field, DataOutput fd, String val) throws XBaseException;
    public XBaseHeader.XBaseField buildField(String name, int fieldLength) throws XBaseException;

    public static final XBaseFieldType[] FIELD_TYPES = {
	new XBaseFieldTypeString(),
	new XBaseFieldTypeNumber(),
	new XBaseFieldTypeLogical(),
	new XBaseFieldTypeDate(),
	new XBaseFieldTypeMemo(),
	new XBaseFieldTypeFloating(),
	new XBaseFieldTypeInteger(),
    };

    public static class XBaseFieldTypeString extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeString() {
	    super('C', 254, "String");
	}
    }

    public static class XBaseFieldTypeNumber extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeNumber() {
	    super('N', 18, "Number");
	}
    }

    public static class XBaseFieldTypeLogical extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeLogical() {
	    super('L', 1, "Logical");
	}
    }

    public static class XBaseFieldTypeDate extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeDate() {
	    super('D', 8, "Date");
	}
    }

    public final static int MEMO_FIELD_LENGTH = 10;

    public static class XBaseFieldMemo extends XBaseHeader.XBaseField {
	private RandomAccessFile dbtFd = null;
	private XBaseHeader header;
	private XBaseMemoFile memoFile = null;

	public XBaseFieldMemo(XBaseFieldType type, String name) throws XBaseException {
	    super(type, name, 10);
	}

	protected void setHeader(XBaseHeader header) throws XBaseException {
	    super.setHeader(header);
	    this.header = header;
	    if (header.getDbtFd() == null)
		throw new XBaseException("There is a memo field but no memo file");
	    memoFile = new XBaseMemoFile(header, header.getDbtFd());
	}

	public XBaseValue read(DataInput dbfFd) throws XBaseException {
	    assert(header != null);
	    /* getting pointer value */
	    byte[] bytes = new byte[MEMO_FIELD_LENGTH];
	    try {
		dbfFd.readFully(bytes);
	    } catch(IOException e) {
		throw new XBaseException("IOException (premature end of file ?)", e);
	    }
	    String str;
	    try {
		str = new String(bytes, header.getCharset());
	    } catch(UnsupportedEncodingException e) {
		throw new XBaseException("Unexpected UnsupportedEncodingException", e);
	    }
	    int ptr;
	    try {
		ptr = Util.parseInt(str);
	    } catch (java.lang.NumberFormatException e) {
		/* throw new XBaseException("Invalid memo field content: '" + str + "'", e); */
		Util.verbose("Invalid memo field content: '" + str + "', assuming no memo");
		return new XBaseValue(this, "");
	    }
	    if ( ptr == 0 )
		return new XBaseValue(this, "");

	    /* getting value from memo file */
	    return new XBaseValue(this, memoFile.getMemo(ptr));
	}

	public void write(DataOutput dbfFd, String val) throws XBaseException {
	    try {
		int ptr;
		if ("".equals(val.trim()))
		    ptr = 0;
		else
		    ptr = memoFile.appendMemo(val);
		val = Integer.toString(ptr);

		byte[] valBytes = val.getBytes();
		int valByteLength; /* excluding \0 */
		for ( valByteLength = 0 ;
		      valByteLength < valBytes.length && valBytes[0] != '\0' ;
		      valByteLength++ );

		byte[] bytes = new byte[MEMO_FIELD_LENGTH];
		for ( int i = 0 ; i < bytes.length ; i++ )
		    bytes[i] = '0';

		int i, j;
		for ( i = bytes.length - valByteLength, j = 0 ;
		      j < valByteLength ; i++, j++ ) {
		    bytes[i] = valBytes[j];
		}

		dbfFd.write(bytes);
	    } catch(IOException e) {
		throw new XBaseException("Field writing failed", e);
	    }
	}
    }

    public static class XBaseFieldTypeMemo extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeMemo() {
	    super('M', 10, "Memo pointer");
	}

	public XBaseHeader.XBaseField buildField(String name, int fieldLength) throws XBaseException {
	    return new XBaseFieldMemo(this, name);
	}

	public int getMaxUserLength() {
	    return 5000;
	}
    }

    public static class XBaseFieldTypeFloating extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeFloating() {
	    super('F', 20, "Floating");
	}
    }

    public static class XBaseFieldTypeInteger extends XBaseFieldTypeDefaultImpl {
	public XBaseFieldTypeInteger() {
	    super('I', 4, "Integer");
	}

	public XBaseValue read(XBaseHeader header, XBaseHeader.XBaseField field, DataInput fd) throws XBaseException {
	    try {
		return new XBaseValue(field, Integer.toString(Integer.reverse(fd.readInt())));
	    } catch(IOException e) {
		throw new XBaseException("IOException (premature end of file ?)", e);
	    }
	}
    }

    public static class XBaseFieldTypeDefaultImpl implements XBaseFieldType {
	private byte byteValue;
	private int maxLength;
	private String userName;

	public XBaseFieldTypeDefaultImpl(char c, int maxLength, String userName) {
	    byteValue = (byte)c;
	    this.maxLength = maxLength;
	    this.userName = userName;
	}

	public byte getByteValue() {
	    return byteValue;
	}

	public String toString() {
	    return userName;
	}

	public XBaseValue read(XBaseHeader header, XBaseHeader.XBaseField field, DataInput fd) throws XBaseException {
	    byte[] bytes = new byte[field.getLength()];
	    try {
		fd.readFully(bytes);
	    } catch (IOException e) {
		throw new XBaseException("IOException (premature end of file ?)", e);
	    }
	    String str;
	    str = Util.trim(Util.toString(bytes, header.getCharset()));
	    return new XBaseValue(field, str);
	}

	public void write(XBaseHeader header, XBaseHeader.XBaseField field, DataOutput fd, String val) throws XBaseException {
	    try {
		byte[] bytes = val.getBytes(header.getCharset());
		if ( bytes.length > field.getLength() ) {
		    throw new XBaseException("Value '" + val + "' too long for field '" + field.getName() + "' (max length: " + Integer.toString(field.getLength()) + ")");
		}

		int i;
		for ( i = 0 ; i < bytes.length && bytes[i] != '\0' ; i++ );
		fd.write(bytes, 0, i);
		byte[] padding = new byte[field.getLength() - i];
		byte p = (byte)header.getPaddingChar();
		for ( i = 0 ; i < padding.length ; i++ )
		    padding[i] = p;
		fd.write(padding);
	    } catch (IOException e) {
		throw new XBaseException("IOException while writing files", e);
	    }
	}

	public int getMaxInternalLength() {
	    return maxLength;
	}

	public int getMaxUserLength() {
	    return getMaxInternalLength();
	}

	public XBaseHeader.XBaseField buildField(String name, int fieldLength) throws XBaseException {
	    return new XBaseHeader.XBaseField(this, name, fieldLength);
	}

	public boolean equals(Object o) {
	    if ( o == null || !(o instanceof XBaseFieldType))
		return false;
	    return o.getClass() == this.getClass();
	}
    }
}
