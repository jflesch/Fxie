package net.kwain.fxie;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.util.List;
import java.util.Vector;

public class XBaseExport
{
    public static interface DataProvider {
	/**
	 * Called to know if there is a row with this number (starts from 0)
	 * @return false indicates end of data
	 */
	public boolean hasRow(int row);

	/**
	 * Callback called to get all the data to write
	 * @note row is always progressvely incremented (no gap, no turning back)
	 */
	public String getValue(int row, XBaseHeader.XBaseField field);
    }

    private File realDbfFile = null;
    private File realDbtFile = null;
    private RandomAccessFile realDbfFd;
    private RandomAccessFile realDbtFd;
    private File tmpDbfFile;
    private File tmpDbtFile;
    private RandomAccessFile tmpDbfFd;
    private RandomAccessFile tmpDbtFd;
    private XBaseHeader header;
    private DataProvider provider;

    public XBaseExport(File writableDbf, File writableDbt,
		       XBaseHeader.XBaseVersion version,
		       XBaseHeader.XBaseCharset charset,
		       List<XBaseHeader.XBaseField> fields,
		       DataProvider provider) throws XBaseException {
	this(writableDbf, writableDbt,
	     version, charset, fields, provider, '\0');
    }

    public XBaseExport(File writableDbf, File writableDbt,
		       XBaseHeader.XBaseVersion version,
		       XBaseHeader.XBaseCharset charset,
		       List<XBaseHeader.XBaseField> fields,
		       DataProvider provider,
		       char paddingChar) throws XBaseException {
	this.realDbfFile = writableDbf;
	this.realDbtFile = writableDbt;
	try {
	    realDbfFd = new RandomAccessFile(writableDbf, "rw");
	    realDbtFd = new RandomAccessFile(writableDbt, "rw");
	} catch(IOException e) {
	    throw new XBaseException("Can't open files", e);
	}
	/* we write first in temporary files in case we are interrupted (we don't want
	 * to corrupt existing data)
	 */
	try {
	    tmpDbfFile = File.createTempFile("tmp-fxie", ".dbf");
	    tmpDbtFile = File.createTempFile("tmp-fxie", ".dbt");
	    Util.verbose("TMP DBF FILE: " + tmpDbfFile.getPath());
	    Util.verbose("TMP DBT FILE: " + tmpDbtFile.getPath());
	    tmpDbfFile.deleteOnExit();
	    tmpDbtFile.deleteOnExit();
	    tmpDbfFd = new RandomAccessFile(tmpDbfFile, "rw");
	    tmpDbtFd = new RandomAccessFile(tmpDbtFile, "rw");
	} catch(IOException e) {
	    throw new XBaseException("Can't create temporary files", e);
	}
	header = new XBaseHeader(tmpDbfFd, tmpDbtFd, version, charset, fields, paddingChar);
	this.provider = provider;
    }

    public void copy(File src, File dst) throws IOException {
	FileChannel inChannel = new FileInputStream(src).getChannel();
        FileChannel outChannel = new FileOutputStream(dst).getChannel();
	Util.verbose(src.getPath() + " -> " + dst.getPath() + " (" + Long.toString(inChannel.size()) + ")");
	inChannel.transferTo(0, inChannel.size(), outChannel);
	inChannel.close();
	outChannel.close();
    }

    public void write() throws XBaseException {
	try {
	    tmpDbfFd.seek(header.getHeaderLength());

	    int row;
	    for ( row = 0 ; provider.hasRow(row) ; row++ ) {
		tmpDbfFd.writeByte(0x20); /* not deleted */
		List<XBaseValue> values = new Vector<XBaseValue>(header.getFields().size());
		for ( XBaseHeader.XBaseField field : header.getFields() ) {
		    String val = provider.getValue(row, field);
		    if ( val == null )
			throw new XBaseException("Invalid value returned by provider (null)");
		    field.write(tmpDbfFd, val);
		}
	    }
	    tmpDbfFd.writeByte(0x1a); /* eof */

	    header.rewriteHeader(row);

	    tmpDbfFd.close();
	    if ( tmpDbtFd != null )
		tmpDbtFd.close();

	    Util.verbose("Squashing the original file with the generated one");
	    realDbfFile.delete();
	    realDbtFile.delete();
	    copy(tmpDbfFile, realDbfFile);
	    copy(tmpDbtFile, realDbtFile);

	    tmpDbfFile.delete();
	    tmpDbtFile.delete();
	} catch (IOException e) {
	    throw new XBaseException("Can't write Xbase files", e);
	}
    }
}