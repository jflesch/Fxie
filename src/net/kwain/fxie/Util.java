package net.kwain.fxie;

public class Util {
    public final static boolean VERBOSE = false;

    /**
     * Custom implementation of String.trim():
     * Trim trailing white spaces, but not leading ones
     */
    public static String trim(String in) {
	int out;
	char[] chars = in.toCharArray();

	for (out = chars.length;
	     out >= 1
		 && (chars[out-1] == ' '
		     || chars[out-1] == '\t'
		     || chars[out-1] == '\n');
	     out--);

	return new String(chars, 0, out);
    }

    public static int parseInt(String str) {
	str = str.trim();
	if ( "".equals(str) )
	    return 0;
	return Integer.parseInt(str);
    }

    public static void verbose(String str) {
	if (VERBOSE) {
	    System.err.println("FXIE: " + str);
	}
    }

    public static String toString(byte[] b, String charset) {
	int end;
	for ( end = 0 ; end < b.length ; end++ ) {
	    if (b[end] == '\0')
		break;
	}
	try {
	    return new String(b, 0, end, charset);
	} catch(java.io.UnsupportedEncodingException e) {
	    throw new RuntimeException("UnsupportedEncodingException for charset: " + charset, e);
	}
    }
}
