package net.kwain.fxie;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Vector;

public class XBaseHeader {
    public final static int XBASE_FIELD_HEADER_LENGTH = 32;

    public enum XBaseVersion {
	/* Note: We only know how to read Dbase III+ memo files */
	XBASE_VERSION_FOXBASE(          "FoxBase",                        0x02, false, false),
	XBASE_VERSION_NO_DBT(           "No DBT",                         0x03, false, false),
	XBASE_VERSION_DBASE_IV_NO_MEMO( "Dbase IV w/o memo",              0x04, false, false),
	XBASE_VERSION_DBASE_V_NO_MEMO(  "Dbase V w/o memo",               0x05, false, false),
	XBASE_VERSION_VISUAL_FOXPRO_DBC("Visual FoxPro w/ DBC",           0x30, false, true),
	XBASE_VERSION_VISUAL_FOXPRO_INC("Visual FoxPro w/ autoincrement", 0x31, false, true),
	XBASE_VERSION_DBV_MEMO_VAR_SIZE("DBV memo var size",              0x43, false, false),
	XBASE_VERSION_DBASE_IV_MEMO(    "Dbase IV w/ memo",               0x7B, false, false),
	XBASE_VERSION_DBASE_IIIP_MEMO(  "Dbase III+ w/ memo",             0x83, true, false),
	XBASE_VERSION_DBASE_IV_MEMO_BIS("Dbase IV w/ memo (bis)",         0x8B, true, false),
	XBASE_VERSION_DBASE_IV_SQL(     "Dbase IV w/ sql",                0x8E, false, false),
	XBASE_VERSION_DBV_DBT_MEMO(     "Dbv and dbt memo",               0xB3, true, false);

	private String name;
	private byte value;
	private boolean memo;
	private boolean hasDatabaseContainer;
	XBaseVersion(String name, int val, boolean hasMemo, boolean hasDatabaseContainer) {
	    this.name = name;
	    this.value = (byte)val;
	    this.memo = hasMemo;
	    this.hasDatabaseContainer = hasDatabaseContainer;
	}
	public byte getByteValue() { return value; }
	public String toString() { return name;	}
	public boolean hasMemo() { return memo; }
	public boolean hasDatabaseContainer() { return hasDatabaseContainer; }
    };

    public enum XBaseCharset {
	CHARSET_DEFAULT(0x00, "Unspecified, assuming DOS USA", "CP437"),
	    CHARSET_DOS_USA(0x01, "DOS USA", "CP437"),
	    CHARSET_DOS_MULTILANGUAL(0x02, "DOS multilingual", "CP850"),
	    CHARSET_WINDOWS_ANSI(0x03, "Windows ANSI", "CP1252"),
	    CHARSET_EE_MSDOS(0x64, "EE-MSDOS", "CP852"),
	    CHARSET_NORDIC_MSDOS(0x65, "Nordic MSDOS", "CP865"),
	    CHARSET_RUSSIAN_MSDOS(0x66, "Russian MSDOS", "CP866"),
	    CHARSER_WINDOWS_EE(0xC8, "Windows EE", "CP1250");

	private byte value;
	private String name;
	private String charsetName;
	XBaseCharset(int value, String name, String charsetName) {
	    this.value = (byte)value;
	    this.name = name;
	    this.charsetName = charsetName;
	}

	public int getByteValue() { return value; }
	public String toString() { return name; }
	/**
	 * Returns the system charset name (usable with java.lang.String / java.nio.charset.Charset)
	 */
	public String getCharsetName() { return charsetName; }
    };

    private XBaseVersion version;
    private XBaseCharset charset;
    private List<XBaseField> fields;
    private int nmbRecords;
    private int headerLength;
    private int recordLength;
    private char paddingChar; /* write only */

    private RandomAccessFile dbfFd;
    private RandomAccessFile dbtFd;

    /**
     * will be able to write the header
     */
    protected XBaseHeader(RandomAccessFile writableDbf, RandomAccessFile writableDbt,
			  XBaseVersion version,
			  XBaseCharset charset,
			  List<XBaseField> fields,
			  char paddingChar) throws XBaseException {
	this.dbfFd = writableDbf;
	this.dbtFd = writableDbt;
	this.version = version;
	this.charset = charset;
	this.fields = fields;
	this.paddingChar = paddingChar;
	this.headerLength = 32;
	this.recordLength = 0;
	for ( XBaseField field : fields ) {
	    field.setHeader(this);
	    this.headerLength += 32;
	    this.recordLength += field.getLength();
	}
	this.headerLength++; /* stop byte */
	if ( version.hasDatabaseContainer() ) {
	    headerLength += 263;
	}
    }

    /**
     * will read header from file
     */
    protected XBaseHeader(RandomAccessFile dbfFd, RandomAccessFile dbtFd) throws XBaseException {
	this.dbfFd = dbfFd;
	this.dbtFd = dbtFd;

	headerLength = 0;
	recordLength = 0;
	try {
	    dbfFd.seek(0);

	    version = parseVersion(dbfFd.readByte()); /* 0 */
	    byte[] lastMod = new byte[3];
	    dbfFd.read(lastMod); /* 1 - 3 */ /* TODO: lastMod parsing */
	    nmbRecords = Integer.reverseBytes(dbfFd.readInt()); /* 4 - 7 */
	    dbfFd.skipBytes(2); /* 8 - 9 */ /* header length (ignored) */
	    dbfFd.skipBytes(2); /* 10 - 11 */ /* record length (ignored) */
	    dbfFd.skipBytes(2); /* 12 - 13 */ /* reserved */
	    dbfFd.skipBytes(1); /* 14 */ /* incomplete transaction (ignored) */
	    dbfFd.skipBytes(1); /* 15 */ /* encryption flag (ignored) */
	    dbfFd.skipBytes(4); /* 16 - 19 */ /* free record thread (ignored) */
	    dbfFd.skipBytes(8); /* 20 - 27 */ /* multi user stuff (ignored) */
	    dbfFd.skipBytes(1); /* 28 */ /* MDX flag (ignored) */
	    charset = parseCharset(dbfFd.readByte()); /* 29 */
	    dbfFd.skipBytes(2); /* 30 - 31 */ /* reserved */

	    Util.verbose("DBT file contains " + Integer.toString(nmbRecords) + " records");
	    Util.verbose("DBT file charset: " + charset.getCharsetName());

	    headerLength += 32;

	    fields = new Vector<XBaseField>();
	    XBaseField field;
	    while( (field = readFieldHeader(dbfFd, charset.getCharsetName())) != null ) {
		fields.add(field);
		headerLength += 32;
		recordLength += field.getLength();
	    }
	    headerLength += 1; /* stop byte */

	    if ( version.hasDatabaseContainer() ) {
		byte[] databaseContainer = new byte[263];
		dbfFd.seek(headerLength + 263);
		headerLength += 263;
	    }
	} catch (IOException e) {
	    throw new XBaseException("IOException", e);
	}
    }

    private static XBaseVersion parseVersion(byte c) throws XBaseException {
	for (XBaseVersion ver : XBaseVersion.values()) {
	    if (ver.getByteValue() == c)
		return ver;
	}
	throw new XBaseException("Unknown file version: " + Byte.toString(c));
    }

    private static XBaseCharset parseCharset(byte c) throws XBaseException {
	for (XBaseCharset charset : XBaseCharset.values()) {
	    if (charset.getByteValue() == c)
		return charset;
	}
	throw new XBaseException("Unknown charset: " + Byte.toString(c));
    }

    public static class XBaseField {
	private XBaseHeader header;
	private XBaseFieldType fieldType;
	private int fieldLength;
	private String name;

	public XBaseField(XBaseFieldType type, String name, int fieldLength)
	    throws XBaseException {
	    this.header = header;
	    this.name = name;
	    this.fieldType = type;
	    this.fieldLength = fieldLength;
	    if (fieldLength > fieldType.getMaxUserLength()) {
		throw new XBaseException("Invalid field length for field '" + name + "': "
					 + " Got length " + Integer.toString(fieldLength)
					 + " when max allowed is "
					 + Integer.toString(fieldType.getMaxUserLength()));
	    }
	}

	protected void setHeader(XBaseHeader header) throws XBaseException {
	    this.header = header;
	}

	public String toString() {
	    return name;
	}

	public String getName() {
	    return name;
	}

	public XBaseFieldType getFieldType() {
	    return fieldType;
	}

	public XBaseValue read(DataInput dbfFd) throws XBaseException {
	    assert(header != null);
	    return getFieldType().read(header, this, dbfFd);
	}

	public void write(DataOutput dbfFd, String val) throws XBaseException {
	    getFieldType().write(header, this, dbfFd, val);
	}

	public int getLength() {
	    return fieldLength;
	}

	public boolean equals(Object o) {
	    if ( o == null || !(o instanceof XBaseField) )
		return false;
	    XBaseField f = (XBaseField)o;
	    return getFieldType().equals(f.getFieldType())
		&& getLength() == f.getLength()
		&& toString().equals(f.toString());
	}

	public int hashCode() {
	    return toString().hashCode();
	}
    }

    private static XBaseFieldType parseFieldType(byte fieldType) throws XBaseException {
	for ( XBaseFieldType type : XBaseFieldType.FIELD_TYPES ) {
	    if ( type.getByteValue() == fieldType )
		return type;
	}
	throw new XBaseException("Unknown field type: " + Character.toString((char)fieldType)
				 + " (" + Byte.toString(fieldType) + ")");
    }

    private XBaseField readFieldHeader(RandomAccessFile dbfFd, String charset)
	throws XBaseException {
	try {
	    dbfFd.seek(headerLength);
	    byte stopByte = dbfFd.readByte();
	    if ( stopByte == 0x0D )
		return null;
	    dbfFd.seek(headerLength);

	    byte[] nameBytes = new byte[11];
	    dbfFd.read(nameBytes); /* 0 - 10 */
	    byte fieldType = dbfFd.readByte();      /* 11 */
	    dbfFd.skipBytes(4); /* 12 - 15 */
	    byte fieldLength = dbfFd.readByte();    /* 16 */
	    dbfFd.skipBytes(1); /* 17 */ /* decimal count */
	    dbfFd.skipBytes(2); /* 18 - 19 */ /* multiuser */
	    dbfFd.skipBytes(1); /* 20 */ /* workAreaId */
	    dbfFd.skipBytes(2); /* 21 - 22 */ /* multiuser */
	    dbfFd.skipBytes(1); /* 23 */ /* setFieldFlag */
	    dbfFd.skipBytes(7); /* 24 - 30 */ /* reserverd */
	    dbfFd.skipBytes(1); /* 31 */ /* index field flag */

	    String name = Util.toString(nameBytes, charset).trim();
	    Util.verbose("Field name: " + name + " (length: " + Integer.toString((int)fieldLength) + ")");
	    XBaseFieldType xfieldType = parseFieldType(fieldType);
	    XBaseField field = xfieldType.buildField(name, (int)fieldLength);
	    field.setHeader(this);
	    return field;
	} catch (IOException e) {
	    throw new RuntimeException("IOException while reading DBF file", e);
	}
    }

    public XBaseVersion getEditorVersion() {
	return version;
    }

    /**
     * Returns the system charset name (usable with java.lang.String and java.nio.charset.Charset for instance)
     */
    public String getCharset() {
	return charset.getCharsetName();
    }

    public List<XBaseField> getFields() {
	return fields;
    }

    public int getNmbRecords() {
	return nmbRecords;
    }

    public int getHeaderLength() {
	return headerLength;
    }

    public int getRecordLength() {
	return recordLength;
    }

    protected RandomAccessFile getDbtFd() {
	return dbtFd;
    }

    public char getPaddingChar() {
	return paddingChar;
    }

    private void writeFieldHeader(DataOutput out, XBaseField field) throws IOException {
	byte[] name = field.getName().getBytes(getCharset());
	assert(name.length <= 10);
	int i = 0;

	for ( ; i < name.length && name[i] != '\0' ; i++ );
	out.write(name, 0, i);
	for ( ; i < 11 ; i++ ) {
	    out.writeByte(0);
	}

	out.writeByte(field.getFieldType().getByteValue());

	for ( i = 0 ; i < 4 ; i++ )
	    out.writeByte(0); /* field data address */

	out.writeByte(field.getLength());

	byte[] blank = new byte[1 /* decimal count */
				+ 2 /* multi user */
				+ 1 /* work area id */
				+ 2 /* multi user */
				+ 1 /* set fields flag */
				+ 7 /* reserverd */
				+ 1 /* index field flag */];
	for ( i = 0 ; i < blank.length ; i++)
	    blank[i] = 0;
	out.write(blank);
    }

    public void rewriteHeader(int recordNumber) throws XBaseException {
	try {
	    dbfFd.seek(0);

	    dbfFd.writeByte(version.getByteValue());
	    for (int i = 0 ; i < 3 ; i++)
		dbfFd.writeByte(0); /* TODO : Last update date */
	    dbfFd.writeInt(Integer.reverseBytes(recordNumber));
	    dbfFd.writeShort(Short.reverseBytes((short)getHeaderLength()));
	    dbfFd.writeShort(Short.reverseBytes((short)getRecordLength()));
	    dbfFd.writeShort(0); /* reserverd */
	    dbfFd.writeByte(0); /* incomplete transaction */
	    dbfFd.writeByte(0); /* encryption flag */
	    for ( int i = 0 ; i < 12 ; i++ )
		dbfFd.writeByte(0); /* Lan + multiuser */
	    dbfFd.writeByte(0); /* MDX flag */
	    dbfFd.writeByte(charset.getByteValue());
	    dbfFd.writeShort(0); /* reserved */

	    for (XBaseField field : fields) {
		writeFieldHeader(dbfFd, field);
	    }
	    dbfFd.writeByte(0xD);
	} catch(IOException e) {
	    throw new XBaseException("Writing failed", e);
	}
    }
}
