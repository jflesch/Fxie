package net.kwain.fxie;

public class XBaseException extends Exception {
    static final long serialVersionUID = 1;

    public XBaseException(String reason) {
	super(reason);
    }

    public XBaseException(String reason, Exception e) {
	super(reason, e);
    }
}
