package net.kwain.fxie;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.io.UnsupportedEncodingException;

public class XBaseMemoFile {
    public final static int BLOCK_SIZE = 512;
    public final static int HEADER_SIZE = 512;

    private RandomAccessFile file;
    private XBaseHeader header;

    protected XBaseMemoFile(XBaseHeader header, RandomAccessFile file) throws XBaseException {
	if (header.getEditorVersion() != XBaseHeader.XBaseVersion.XBASE_VERSION_DBASE_IIIP_MEMO)
	    throw new XBaseException("Sorry, only Dbase III+ memo files are supported for now");
	this.file = file;
	this.header = header;
    }

    private long computePosition(int ptr) {
	return ((((long)ptr)-1) * BLOCK_SIZE) + HEADER_SIZE;
    }

    private boolean parseAndAppend(StringBuilder builder, byte[] input)
	throws IOException, UnsupportedEncodingException {

	int start = 0;

	for ( int i = 0 ; i < input.length ; i++ ) {
	    if ( input[i] == 0x8D && i+1 < input.length && input[i+1] == 0x0A ) {
		/* line return */
		if ( i - start > 0 ) {
		    String str = new String(input, start, i, header.getCharset());
		    builder.append(str);
		}
		start = i+2;
		i++; /* skip 0x8D and 0x0A */

	    } else if ( input[i] == 0x1A || input[i] == 0x0 ) {
		/* end of memo */
		if ( i - start > 0 ) {
		    String str = new String(input, start, i, header.getCharset());
		    builder.append(str);
		}
		return true;
	    }
	}

	return false;
    }

    public String getMemo(int ptr) throws XBaseException {
	StringBuilder strBuilder = new StringBuilder();
	byte[] bytes = new byte[BLOCK_SIZE];

	try {
	    file.seek(computePosition(ptr));
	} catch (IOException e) {
	    throw new XBaseException("IOException while reading memo file (premature end of file ?), "
				     + "was looking for memo " + Integer.toString(ptr), e);
	}

	try {
	    while(true) {
		file.readFully(bytes);
		if (parseAndAppend(strBuilder, bytes))
		    break;
	    }
	} catch (IOException e) {
	    throw new XBaseException("IOException while reading memo file (premature end of file ?), "
				     + "was looking for memo " + Integer.toString(ptr)
				     + " (position " + Long.toString(computePosition(ptr)) + ")", e);
	}

	return strBuilder.toString();
    }

    private int lastPtr = -1;

    private int write(int written, byte[] bytes) throws IOException {
	for ( int i = 0 ; i < bytes.length ; i++ ) {
	    if ( bytes[i] == '\n' ) {
		file.writeByte(0x8D);
		file.writeByte(0x0A);
		written += 2;
	    } else {
		file.writeByte(bytes[i]);
		written++;
	    }

	    if ( written >= 512 ) {
		lastPtr++;
		written -= 512;
	    }
	}
	return written;
    }

    /**
     * Don't mix with getMemo() !
     */
    public int appendMemo(String str) throws XBaseException {
	try {
	    int firstPtr;

	    if ( lastPtr < 0 ) {
		file.seek(0);
		/* reset the header */
		for (int i = 0 ; i < HEADER_SIZE ; i++ ) {
		    if ( i == 16 )
			file.writeByte(0x03); /* version */
		    else
			file.writeByte('\0');
		}
		lastPtr=0;
	    }
	    lastPtr++;

	    firstPtr = lastPtr;

	    file.seek(computePosition(lastPtr));

	    byte[] bytes = str.getBytes(header.getCharset());
	    int i = write(0, bytes);
	    i = write(i, new byte[] { 0x1A, 0x0 });

	    for ( ; i < 512 ; i++ ) {
		file.writeByte(0x0);
	    }

	    Util.verbose("Added memo " + Integer.toString(firstPtr));
	    return firstPtr;
	} catch (IOException e) {
	    throw new XBaseException("IOException while writing files", e);
	}
    }
}
