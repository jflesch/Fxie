package net.kwain.fxie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;
import java.util.Vector;

public class XBaseImport
{
    private RandomAccessFile dbfFd, dbtFd;
    private XBaseHeader header;

    private int currentLine = 0; /* line that *will* be read */

    /**
     * dbt can be null if dbf does not contain any memo field
     */
    public XBaseImport(File dbf, File dbt) throws XBaseException, FileNotFoundException {
	this(new RandomAccessFile(dbf, "r"),
	     (dbt != null && dbt.exists() ? new RandomAccessFile(dbt, "r") : null ) );
    }

    public XBaseImport(RandomAccessFile dbfReadable, RandomAccessFile dbtReadable) throws XBaseException, FileNotFoundException {
	dbfFd = dbfReadable;
	dbtFd = dbtReadable;
	header = new XBaseHeader(dbfFd, dbtFd);
    }

    public XBaseHeader getHeader() {
	return header;
    }

    /**
     * Note: this function use the Dbase header to compute its result, and some
     * tools/database seem to fill in the header poorly. You have also to check
     * that read() doesn't return null
     * @return number of remaining records
     */
    public int available() {
	return header.getNmbRecords() - currentLine;
    }

    public int getLinePosition() {
	return currentLine;
    }

    private long computePosition(int line) throws XBaseException {
	if ( line >= header.getNmbRecords() )
	    throw new XBaseException("Requested line out of bounds");
	return ((long)header.getHeaderLength()) + (((long)line) * ((long)header.getRecordLength()+1));
    }

    /**
     * @param line (starts counting from 0)
     */
    public void setLinePosition(int line) throws XBaseException {
	try {
	    dbfFd.seek(computePosition(line));
	} catch(IOException e) {
	    throw new XBaseException("IOException", e);
	}
	currentLine = line;
    }

    /**
     * alias for setLinePosition()
     */
    public void seek(int line) throws XBaseException {
	setLinePosition(line);
    }

    /**
     * Returns an Xbase line / a record
     */
    public List<XBaseValue> read() throws XBaseException {
	byte firstByte = 0;
	boolean deleted = false;
	boolean valuesRead = false;

	setLinePosition(currentLine);

	List<XBaseValue> values = new Vector<XBaseValue>(header.getFields().size());
	try {
	    while ( (deleted = ((firstByte = dbfFd.readByte()) == 0x2A)) || !valuesRead ) {
		if ( firstByte == 0 || firstByte == 0x1A ) {
		    Util.verbose("Unexpected EOF (is header wrong ?)");
		    return null;
		}
		for (XBaseHeader.XBaseField field : header.getFields()) {
		    try {
			XBaseValue value = field.read(dbfFd);
			if (!deleted)
			    values.add(value);
		    } catch (XBaseException e) {
			Util.verbose("Error happened while reading record " + Integer.toString(currentLine));
			Util.verbose("Was able to read from this record: ");
			for ( XBaseValue v : values ) {
			    Util.verbose(v.getField().getName() + ": " + v.getHumanReadableValue());
			}
			throw e;
		    }
		}
		if (!deleted)
		    valuesRead = true;
	    }
	} catch(IOException e) {
	    throw new XBaseException("IOException: missing records (premature end of file ?)", e);
	}

	currentLine++;
	return values;
    }

    public void close() {
	try {
	    dbfFd.close();
	    if ( dbtFd != null )
		dbtFd.close();
	} catch (IOException e) {
	    /* only case where it should happen is if the "user" calls this method twice,
	     * -> not very common.
	     */
	    throw new RuntimeException("IOException", e);
	}
    }
}
