package net.kwain.fxie;

public class XBaseValue {
    private XBaseHeader.XBaseField field;
    private String value;

    protected XBaseValue(XBaseHeader.XBaseField field, String value) {
	this.field = field;
	this.value = value;
    }

    public XBaseHeader.XBaseField getField() {
	return field;
    }

    public String getHumanReadableValue() {
	return value;
    }
}
